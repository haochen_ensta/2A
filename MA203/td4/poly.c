#include "poly.h"

unsigned int Log256[256] = {
  // A completer
};

unsigned int Alog256[256] = {
  // A completer
};

unsigned int add256(unsigned int a, unsigned int b) {
  return a^b;
}

unsigned int mul256(unsigned int a, unsigned int b) {
  // A completer
}

unsigned int div256(unsigned int a, unsigned int b) {
  // A completer
}

unsigned int inv256(unsigned int a) {
  // A completer
}

poly* poly_init() {
  poly* P = malloc(sizeof(poly));
  P->deg = -1;
  P->coeff = NULL;
  return P;
}

void poly_add(poly* res, poly* a, poly* b) {
  int i;
  poly* tmp;
  // a est le polynome de plus haut degre: on inverse si necessaire
  if (a->deg < b->deg) {
    tmp = a;
    a = b;
    b = tmp;
  }
  if (a->deg == -1) {
    // somme de deux polynomes nuls
    res->deg = -1;
    if (res->coeff) {
      free(res->coeff);
    }
    res->coeff = NULL;
    return;
  }
  tmp = poly_init();
  tmp->coeff = (unsigned int*) malloc((a->deg+1)*sizeof(unsigned int));
  for (i=0; i<=b->deg; i++) {
    tmp->coeff[i] = add256(a->coeff[i], b->coeff[i]);
  }
  for (i=b->deg+1; i<=a->deg; i++) {
    tmp->coeff[i] = a->coeff[i];
  }
  i = a->deg;
  while ((i>=0) && (tmp->coeff[i] == 0)) {
    i--;
  }
  if (i < 0) {
    free(tmp->coeff);
    free(tmp);
    res->deg = -1;
    if (res->coeff) {
      free(res->coeff);
    }
    res->coeff = NULL;
    return;
  }
  if (i != a->deg) {
    tmp->coeff = (unsigned int*) realloc(tmp->coeff, (i+1)*sizeof(unsigned int));
  }
  // on recopie le resultat dans res
  res->deg = i;
  if (res->coeff) {
    free(res->coeff);
  }
  res->coeff = tmp->coeff;
  free(tmp);
}

void poly_scalar_mul(poly* res, poly* a, unsigned int c) {
  int i;
  poly* tmp = poly_init();
  if (c != 0) {
    for (i=0; i<= a->deg; i++) {
      poly_add_monomial(tmp, mul256(c,a->coeff[i]), i);
    }
  }
  poly_copy(res, tmp);
  poly_free(tmp);
}

void poly_mul(poly* res, poly* a, poly* b) {
  int i,j;
  poly* tmp;
  if ((a->deg == -1) || (b->deg == -1)) {
    res->deg = -1;
    if (res->coeff) {
      free(res->coeff);
    }
    res->coeff = NULL;
    return;
  }
  tmp = poly_init();
  tmp->deg = a->deg+b->deg;
  tmp->coeff = (unsigned int*) calloc((tmp->deg+1), sizeof(unsigned int));

  for (i=0; i<=a->deg; i++) {
    for (j=0; j<=b->deg; j++) {
      tmp->coeff[i+j] = add256(tmp->coeff[i+j], mul256(a->coeff[i], b->coeff[j]));
    }
  }
  poly_copy(res, tmp);
  poly_free(tmp);
}

void poly_mod(poly* res, poly* a, poly* b) {
  int i,j;
  unsigned int c;
  poly* tmp;
  if (b->deg == -1) {
    fprintf(stderr,"Division par un polynome nul !\n");
    return;
  }
  tmp = poly_init();
  tmp->coeff = (unsigned int*) malloc((a->deg+1)*sizeof(unsigned int));
  for (i=0; i<=a->deg; i++) {
    tmp->coeff[i] = a->coeff[i];
  }

  if (a->deg < b->deg) {
    res->deg = a->deg;
    if (res->coeff) {
      free(res->coeff);
    }
    res->coeff = tmp->coeff;
    free(tmp);
    return;
  }

  for (i=a->deg; i>=b->deg; i--) {
    c = div256(tmp->coeff[i], b->coeff[b->deg]);
    for (j=0; j<=b->deg; j++) {
      tmp->coeff[i-b->deg+j] = add256(tmp->coeff[i-b->deg+j], mul256(b->coeff[j],c));
    }
  }

  i = b->deg-1;
  while ((i>=0) && (tmp->coeff[i] == 0)) {
    i--;
  }

  if (i<0) {
    res->deg = -1;
    if (res->coeff) {
      free(res->coeff);
    }
    res->coeff = NULL;
    free(tmp->coeff);
    free(tmp);
    return;
  }

  res->deg = i;
  if (res->coeff) {
    free(res->coeff);
  }
  res->coeff = (unsigned int*) realloc(tmp->coeff, (i+1)*sizeof(unsigned int));
  free(tmp);
}

unsigned int poly_coeff(poly* a, int deg) {
  if ((a->deg < deg) || (deg < 0)) {
    return 0;
  }
  return a->coeff[deg];
}

void poly_div(poly* res, poly* a, poly* b) {
  int i;
  unsigned int c;
  if (b->deg == -1) {
    fprintf(stderr, "Division par le polynome nul !\n");
    return;
  }
  poly* tmp1 = poly_init();
  poly* tmp2 = poly_init();
  poly* quot = poly_init();

  poly_copy(tmp1, a);
  for (i=a->deg-b->deg; i>=0; i--) {
    // c est le coefficient de degre i du quotient
    c = div256(poly_coeff(tmp1, i+b->deg),b->coeff[b->deg]);
    poly_add_monomial(quot, c, i);
    poly_null(tmp2);
    poly_add_monomial(tmp2, c, i);
    poly_mul(tmp2, b, tmp2);
    poly_add(tmp1, tmp1, tmp2);
  }
  poly_copy(res, quot);
  poly_free(tmp1);
  poly_free(tmp2);
  poly_free(quot);
}

void poly_copy(poly* res, poly* a) {
  unsigned int* tmp;
  int i;
  res->deg = a->deg;
  if (res->deg == -1) {
    if (res->coeff) {
      free(res->coeff);
    }
    res->coeff = NULL;
    return;
  }

  tmp = (unsigned int*) malloc((a->deg+1)*sizeof(unsigned int));
  for (i=0; i<=a->deg; i++) {
    tmp[i] = a->coeff[i];
  }
  if (res->coeff) {
    free(res->coeff);
  }
  res->coeff = tmp;
}

void poly_free(poly* a) {
  if (a->coeff) {
    free(a->coeff);
  }
  free(a);
}

void poly_add_monomial(poly* res, unsigned int coeff, int deg) {
  int i;
  if (coeff == 0) {
    return;
  }
  if (res->deg < deg) {
    res->coeff = (unsigned int*) realloc(res->coeff, (deg+1)*sizeof(unsigned int));
    for (i=res->deg+1; i<deg; i++) {
      res->coeff[i] = 0;
    }
    res->coeff[deg] = coeff;
    res->deg = deg;
    return;
  }
  res->coeff[deg] = add256(res->coeff[deg],coeff);
  i = res->deg;
  while ((i>=0) && (res->coeff[i] == 0)) {
    i--;
  }
  if (i < 0) {
    res->deg = -1;
    if (res->coeff) {
      free(res->coeff);
    }
    res->coeff = NULL;
    return;
  }
  if (i != res->deg) {
    res->coeff = (unsigned int*) realloc(res->coeff, (i+1)*sizeof(unsigned int));
  }
  res->deg = i;
}


void poly_print(poly *a) {
  int i;
  int first = 1;

  for (i=0; i<=a->deg; i++) {
    if (a->coeff[i] != 0) {
      if (!first) {
	printf(" + ");
      } else {
	first = 0;
      }
      if (i==0) {
	printf("%u", a->coeff[i]);
      } else if (i==1) {
	if (a->coeff[i] == 1) {
	  printf("X");
	} else {
	  printf("%uX", a->coeff[i]);
	}
      } else {
	if (a->coeff[i] == 1) {
	  printf("X^%d", i);
	} else {
	  printf("%uX^%d", a->coeff[i], i);
	}
      }
    }
  }
  if (first) {
    printf("0");
  }
  printf("\n");
}

void poly_null(poly* a) {
  a->deg = -1;
  if (a->coeff) {
    free(a->coeff);
  }
  a->coeff = NULL;
}


void kernel(unsigned int** B, int dim, unsigned int*** kernel, int* kernel_dim) {
  int i,j,k,l;
  unsigned int c;
  unsigned int* tmpui;

  // on utilise la matrice I pour trouver le noyau
  unsigned int** I = (unsigned int**) malloc(dim*sizeof(unsigned int*));
  for (i=0; i<dim; i++) {
    I[i] = (unsigned int*) calloc(dim,sizeof(unsigned int));
    I[i][i] = 1;
  }

  i=0; // indice de la ligne du pivot
  j=0; // indice de la colonne du pivot

  while (j<dim) {
    k=i; // k est la premiere ligne de B ayant un coefficient non-nul dans la colonne j
    while ((k<dim) && (B[k][j] == 0)) {
      k++;
    }
    if (k == dim) { // si la colonne j ne contient plus que des 0, on passe a la suivante
      j++;
      continue;
    }
    if (k != i) { // on echange les lignes si necessaire (et aussi dans I)
      tmpui = B[i]; B[i] = B[k]; B[k] = tmpui;
      tmpui = I[i]; I[i] = I[k]; I[k] = tmpui;
    }
    for (k=i+1; k<dim; k++) {
      c = div256(B[k][j],B[i][j]);
      for (l=0; l<dim; l++) {
	B[k][l] = add256(B[k][l],mul256(B[i][l],c));
	I[k][l] = add256(I[k][l],mul256(I[i][l],c));
      }
    }
    i++; // on passe a la ligne d'apres
    j++; // on avance aussi d'une colonne, car on vient de mettre la precedente a 0
  }

  // le pivot est termin�, on recopie ce qu'il faut dans kernel et kernel_dim
  *kernel_dim = dim - i;
  *kernel = (unsigned int**) malloc((*kernel_dim) * sizeof(unsigned int*));
  for (j=0; j<(*kernel_dim); j++) {
    (*kernel)[j] = I[i+j];
  }

  // on lib�re la m�moire qui ne sert plus
  for (j=0; j<i; j++) {
    free(I[j]);
  }
  free(I);

}

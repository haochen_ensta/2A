#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// a^b mod q
unsigned int exponent(unsigned int a, unsigned
  int b, unsigned int q) {
  unsigned int i = 0, res = 1;
  for ( ; i < b ; ++i) {
    res *= a;
    res %= q;
  }
  return res;
}

// a in GF(q)* with q, a prime number
// q - 1 = prod(p[i]^e[i] for i in [[1, k]])
// return order of a
unsigned int order(unsigned int q, unsigned int k,
  unsigned int p[], unsigned int e[], unsigned int a){
  unsigned int t = q - 1;
  unsigned int i = 0;
  unsigned int a_tmp;
  for ( ; i < k ; ++i) {
    t /= (unsigned int)pow(p[i], e[i]);
    a_tmp = exponent(a, t, q);
    while ( a_tmp != 1 ) {
      a_tmp = exponent(a_tmp, p[i], q);
      t *= p[i];
    }
  }
  return t;
}

int main() {
  unsigned int g = 3;
  unsigned int q = 43;
  unsigned int p[] = {2,3,7};
  unsigned int e[] = {1,1,1};
  unsigned int k = 3;
  unsigned int i = 1;
  unsigned int a = g;
  printf("orders of all a in GF(43)* :\n\n");
  for ( ; a != 1 ; (a = (a*g % q)) && (++i))
    printf("ord(%u^%u) = ord(%u) = %u \n", g, i, a, order(q, k, p, e, a));

  return 0;
}
